<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220415105659 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE competence (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competence_since (competence_id INT NOT NULL, since_id INT NOT NULL, INDEX IDX_851DCFF415761DAB (competence_id), INDEX IDX_851DCFF420473888 (since_id), PRIMARY KEY(competence_id, since_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE since (id INT AUTO_INCREMENT NOT NULL, since VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competence_since ADD CONSTRAINT FK_851DCFF415761DAB FOREIGN KEY (competence_id) REFERENCES competence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE competence_since ADD CONSTRAINT FK_851DCFF420473888 FOREIGN KEY (since_id) REFERENCES since (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE competence_since DROP FOREIGN KEY FK_851DCFF415761DAB');
        $this->addSql('ALTER TABLE competence_since DROP FOREIGN KEY FK_851DCFF420473888');
        $this->addSql('DROP TABLE competence');
        $this->addSql('DROP TABLE competence_since');
        $this->addSql('DROP TABLE since');
    }
}
