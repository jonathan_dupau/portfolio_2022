<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220425133508 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE society (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE experiences ADD society_id INT NOT NULL');
        $this->addSql('ALTER TABLE experiences ADD CONSTRAINT FK_82020E70E6389D24 FOREIGN KEY (society_id) REFERENCES society (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_82020E70E6389D24 ON experiences (society_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE experiences DROP FOREIGN KEY FK_82020E70E6389D24');
        $this->addSql('DROP TABLE society');
        $this->addSql('DROP INDEX UNIQ_82020E70E6389D24 ON experiences');
        $this->addSql('ALTER TABLE experiences DROP society_id');
    }
}
