async function init () {
    const node = document.querySelector("#type-text")

    await sleep(1000)
    node.innerText = ""
    await node.type('Développeur web : ')

    while (true) {
        await node.type('FullStack !')
        await sleep(2000)
        await node.delete('FullStack !')
        await node.type('Autodidacte !')
        await sleep(2000)
        await node.delete('Autodidacte !')
        await node.delete('Développeur web : ')
        await node.type('Streamer Développement web')
        await sleep(2000)
        await node.delete('Streamer Développement web')
        await node.type('Développeur web : ')
    }
}


// Source code 🚩

const sleep = time => new Promise(resolve => setTimeout(resolve, time))

class TypeAsync extends HTMLSpanElement {
    get typeInterval () {
        const randomMs = 100 * Math.random()
        return randomMs < 50 ? 10 : randomMs
    }

    async type (text) {
        for (let character of text) {
            this.innerText += character
            await sleep(this.typeInterval)
        }
    }

    async delete (text) {
        for (let character of text) {
            this.innerText = this.innerText.slice(0, this.innerText.length -1)
            await sleep(this.typeInterval)
        }
    }
}

customElements.define('type-async', TypeAsync, { extends: 'span' })


init()
