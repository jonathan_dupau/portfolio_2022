<?php

namespace App\Entity;

use App\Repository\MissionsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MissionsRepository::class)]
class Missions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $mission;

    #[ORM\ManyToOne(targetEntity: Experiences::class, inversedBy: 'missions')]
    private $experiences;


    public function __toString()
    {
        return $this->mission;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMission(): ?string
    {
        return $this->mission;
    }

    public function setMission(string $mission): self
    {
        $this->mission = $mission;

        return $this;
    }

    public function getExperiences(): ?Experiences
    {
        return $this->experiences;
    }

    public function setExperiences(?Experiences $experiences): self
    {
        $this->experiences = $experiences;

        return $this;
    }

}
