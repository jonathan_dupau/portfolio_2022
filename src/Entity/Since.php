<?php

namespace App\Entity;

use App\Repository\SinceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SinceRepository::class)]
class Since
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $since;

    #[ORM\ManyToMany(targetEntity: Competence::class, mappedBy: 'since')]
    private $competences;

    public function __construct()
    {
        $this->competences = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->since;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSince(): ?string
    {
        return $this->since;
    }

    public function setSince(string $since): self
    {
        $this->since = $since;

        return $this;
    }

    /**
     * @return Collection<int, Competence>
     */
    public function getCompetences(): Collection
    {
        return $this->competences;
    }

    public function addCompetence(Competence $competence): self
    {
        if (!$this->competences->contains($competence)) {
            $this->competences[] = $competence;
            $competence->addSince($this);
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): self
    {
        if ($this->competences->removeElement($competence)) {
            $competence->removeSince($this);
        }

        return $this;
    }
}
