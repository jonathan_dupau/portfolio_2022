<?php

namespace App\Entity;

use App\Repository\CompetenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompetenceRepository::class)]
class Competence
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToMany(targetEntity: Since::class, inversedBy: 'competences')]
    private $since;

    #[ORM\Column(type: 'string', length: 255)]
    private $img;

    public function __construct()
    {
        $this->since = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Since>
     */
    public function getSince(): Collection
    {
        return $this->since;
    }

    public function addSince(Since $since): self
    {
        if (!$this->since->contains($since)) {
            $this->since[] = $since;
        }

        return $this;
    }

    public function removeSince(Since $since): self
    {
        $this->since->removeElement($since);

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }
}
