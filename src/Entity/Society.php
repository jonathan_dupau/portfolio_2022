<?php

namespace App\Entity;

use App\Repository\SocietyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SocietyRepository::class)]
class Society
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $adress;

    #[ORM\OneToOne(mappedBy: 'society', targetEntity: Experiences::class, cascade: ['persist', 'remove'])]
    private $experiences;

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getExperiences(): ?Experiences
    {
        return $this->experiences;
    }

    public function setExperiences(Experiences $experiences): self
    {
        // set the owning side of the relation if necessary
        if ($experiences->getSociety() !== $this) {
            $experiences->setSociety($this);
        }

        $this->experiences = $experiences;

        return $this;
    }
}
