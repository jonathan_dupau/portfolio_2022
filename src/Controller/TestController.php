<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    public $prix;
    public $lst_coins = [];

    public function optimalChange($prix)
    {
        $coins =[10, 5, 2];
        $i = 0;
        while ($prix > 0) {
            if($prix >= $coins[$i]){
            $prix -= $coins[$i];
            array_push($this->lst_coins, $coins[$i]);
            } else {
                $i++;
            }
        }
        return $this->lst_coins;
    }

    #[Route('/test', name: 'app_test')]
    public function index(): Response
    {

        $test = $this->optimalChange(7);

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
            'test' => $test
        ]);
    }
}


