<?php

namespace App\Controller;

use App\Entity\Projects;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjetController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/projet/{slug}", name="app_projet")
     */
    public function index(Request $request): Response
    {
        // $projects = $this->entityManager->getRepository(Projects::class)->findAll();
        $projectSlug = $request->attributes->get('slug');
        $project = $this->entityManager->getRepository(Projects::class)->findOneBy([
            'slug' => $projectSlug
        ]);

        $projectId = $project->getId();

        $conn = $this->entityManager->getConnection();

        $sql = '
            SELECT p.name, t.name FROM technos t INNER JOIN projects_technos pt on pt.technos_id = t.id INNER JOIN projects p on pt.projects_id = p.id WHERE p.id = :id ORDER BY pt.projects_id;
            ';
        $stmt = $conn->prepare($sql);
        $technos = $stmt->executeQuery(['id' => $projectId]);
        $technos = $technos->fetchAllAssociative();

        return $this->render('projet/index.html.twig', [
            'project' => $project,
            'technos' => $technos
        ]);
    }
}
