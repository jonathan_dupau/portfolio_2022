<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class CvController extends AbstractController
{
    #[Route('/cv', name: 'app_cv')]
    public function index(): Response
    {
        $cv = new BinaryFileResponse('./uploads/cv.pdf');
        $cv->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'cv.pdf');
        $cvFileName = $cv->getFile()->getFilename();
        $projectRoot = $this->getParameter('kernel.project_dir');
        return $this->file( $projectRoot.'/public/uploads/'.$cvFileName );

    }
}
