<?php

namespace App\Controller;

use App\Entity\Projects;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;


class ProjectsController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/mes-projets', name: 'app_projects')]
    public function index(): Response
    {
        // $projects = $this->entityManager->getRepository(Projects::class)->findAll();

        $conn = $this->entityManager->getConnection();

        $sql = '
            SELECT p.id, p.img, p.big_img, p.url, p.slug, ty.name AS type, p.name AS projet, GROUP_CONCAT(t.name SEPARATOR " | ") AS technos 
            FROM technos t 
            INNER JOIN projects_technos pt on pt.technos_id = t.id 
            INNER JOIN projects p on pt.projects_id = p.id 
            INNER JOIN type ty on p.type_id = ty.id 
            GROUP BY p.id 
            ORDER BY p.created DESC ;
            ';
        $stmt = $conn->prepare($sql);
        $projects = $stmt->executeQuery();
        $projects = $projects->fetchAllAssociative();


        return $this->render('projects/index.html.twig', [
            'projects' => $projects,
        ]);
    }
    
}
