<?php

namespace App\Controller;

use App\Entity\Competence;
use App\Entity\Experiences;
use App\Entity\Projects;
use App\Repository\CompetenceRepository;
use App\Repository\ExperiencesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'home')]
    public function index(CompetenceRepository $competenceRepository, ExperiencesRepository $experiencesRepository): Response
    {
        $projects = $this->entityManager->getRepository(Projects::class)->findBy(array(), array('created' => 'DESC'), 3, null);

        $competences = $competenceRepository->findAll();

        $experiences = $experiencesRepository->findAll();


        return $this->render('home/index.html.twig', [
            'projects' => $projects,
            'competences' => $competences,
            'experiences' => $experiences,
        ]);
    }
}
