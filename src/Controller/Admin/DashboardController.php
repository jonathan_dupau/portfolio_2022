<?php

namespace App\Controller\Admin;

use App\Entity\Competence;
use App\Entity\Experiences;
use App\Entity\Missions;
use App\Entity\Projects;
use App\Entity\Since;
use App\Entity\Society;
use App\Entity\Technos;
use App\Entity\Type;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();
        return $this->render('admin/index.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Portfolio');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Home', 'fas fa-home', 'home');
        yield MenuItem::section('--------');
        yield MenuItem::section('Mon Profil');
        yield MenuItem::linkToCrud('Compétences', 'fa-solid fa-book', Competence::class);
        yield MenuItem::linkToCrud('Durée', 'fa-solid fa-calendar-days', Since::class);
        yield MenuItem::section('--------');
        yield MenuItem::linkToCrud('Expériences', 'fa-solid fa-chess-king', Experiences::class);
        yield MenuItem::linkToCrud('Missions', 'fa-solid fa-bullseye', Missions::class);
        yield MenuItem::linkToCrud('Sociétés', 'fa-solid fa-building-columns', Society::class);
        yield MenuItem::section('--------');
        yield MenuItem::section('Projets');
        yield MenuItem::linkToCrud('Projets', 'fas fa-cube', Projects::class);
        yield MenuItem::linkToCrud('Type', 'fas fa-list', Type::class);
        yield MenuItem::linkToCrud('Technos', 'fas fa-list', Technos::class);
        yield MenuItem::section('--------');
        yield MenuItem::section('Utilisateurs');
        yield MenuItem::linkToCrud('Users', 'fas fa-user', User::class);
    }
}
