<?php

namespace App\Controller\Admin;

use App\Entity\Competence;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CompetenceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Competence::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            ImageField::new('img')
                // fichier de destination
                ->setBasePath('uploads/')
                // fichier de destination depuis la racine
                ->setUploadDir('public/uploads/')
                //  randomisation du nom des images
                ->setUploadedFileNamePattern('[randomhash].[extension]'),
            AssociationField::new('since')->autocomplete(),
        ];
    }

}
