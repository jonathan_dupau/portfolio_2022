<?php

namespace App\Controller\Admin;

use App\Entity\Since;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SinceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Since::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
