<?php

namespace App\Controller\Admin;

use App\Entity\Projects;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class ProjectsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Projects::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('url'),
            // slug du nom de produit
            SlugField::new('slug')->setTargetFieldName('name'),
            'short_description',
            'description',
            'problems',
            'solutions',
            ImageField::new('img')
            // fichier de destination
            ->setBasePath('uploads/')
                // fichier de destination depuis la racine
                ->setUploadDir('public/uploads/')
                //  randomisation du nom des images 
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),
            ImageField::new('bigImg')
            // fichier de destination
            ->setBasePath('uploads/')
                // fichier de destination depuis la racine
                ->setUploadDir('public/uploads/')
                //  randomisation du nom des images 
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),
            AssociationField::new('type')->autocomplete(),
            AssociationField::new('techno')->autocomplete(),
            DateTimeField::new('created'),
            DateTimeField::new('updated'),
        ];
    }
}
